void mechanism(byte per,byte f1,byte r1,byte f2,byte r2){
if((f1==1 && r1==1)||(f1==0&&r1==0)){
  digitalWrite(m1_f,LOW);
  digitalWrite(m1_r,LOW);
  }
else if (f1==1&&r1==0){
  digitalWrite(m1_f,HIGH);
  digitalWrite(m1_r,LOW);
  }
else if (f1==0&&r1==1){
  digitalWrite(m1_f,LOW);
  digitalWrite(m1_r,HIGH);
  }

if((f2==1 && r2==1)||(f2==0&&r2==0)){
  digitalWrite(m2_f,LOW);
  digitalWrite(m2_r,LOW);
  }
else if (f2==1&&r2==0){
  digitalWrite(m2_f,HIGH);
  digitalWrite(m2_r,LOW);
  }
else if (f2==0&&r2==1){
  
  digitalWrite(m2_f,LOW);
  digitalWrite(m2_r,HIGH);
  }

}
/////////////////////motions///////////////////////
void gripper_close(int value){
mechanism(value,1,0,0,0);
}
void gripper_open(int value){
mechanism(value,0,1,0,0);
}
void mechanism_up(int value){
  mechanism(value,0,0,1,0);
  }
  void mechanism_down(int value){
  mechanism(value,0,0,0,1);
  }

void mechanism_stop(){
  mechanism(0,0,0,0,0);
  }
  
