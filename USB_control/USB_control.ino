//this is left arduino
#include <EEPROM.h>
#include <Servo.h>

//**********************thrusters**************************
Servo mh1,mh2,mh3,mv1,mv2,mv3;
//******************mechanism******************************

#define m1_f 10
#define m1_r 9

#define m2_f 11
#define m2_r 5

#define mech_speed 255 
//************************** camera box ************************************
Servo camera;

#define led 6
//************************************************
String control; // this string is control string 
//*******************flags*****************************
boolean last_state=true;
boolean state=false;
//****************thrusters****************************
#define r_speed_h1 initial_max  
#define f_speed_h1 initial_min

#define r_speed_h2 initial_max 
#define f_speed_h2 initial_min

#define r_speed_v1 initial_max
#define f_speed_v1 initial_min

//maximum is 1864
//minimum is 1064
//These values are according to BlueRobotics Github documentation.
//#define initial_max 1664
//#define initial_min 1264
#define initial_max 1864
#define initial_min 1064

