void Control(){
int n_motion=0;
int len = control.length();
char motion;
int k=0;
for(int x=0;x<len;x++){
  if(!isDigit(control[x])){
  n_motion++;
  }
}
  for(int i=0;i<n_motion;i++){// n_motion is presnt # of motions in one string
  motion= control[i+k];
  int value;
  if(len == 4)
  {
    value = (control[i+k+1]-'0')*100+(control[i+k+2]-'0')*10+(control[i+k+3]-'0');
  }
  else if(len == 3)
  {
    value = (control[i+k+1]-'0')*10+(control[i+k+2]-'0');
  }
  else if(len == 2)
  {
    value = (control[i+k+1]-'0');
  }
  
  switch(motion){
   ////////////////////////thruster motions/////////////////////////////
    case('w'):
    rov_forward(value);
    break;
    
    case('s'):
    rov_backward(value);
    break;
    
    case('d'):
    rov_lateral_r(value);
    break;
    
    case('a'):
    rov_lateral_l(value);
    break;
    
    case('e'):
    rov_rotate_cw(value);
    break;

    case('q'):
    rov_rotate_ccw(value);
    break;
    
    case('t'):
    rov_ForwardDiffR(value);
    break;

    case('y'):
    rov_ForwardDiffL(value);
    break;
    
    case('u'):
    rov_up(value);
    break;
    
    case('n'):
    rov_down(value);
    break;
    
//    case('h'):
//    rov_horse(value);
//    break;
//    
//    case('m'):
//    rov_ampleh(value);
//    break;
//    
//    case('l'):
//    rov_dolfin(value);
//    break;
//    
//    case('t'):
//    rov_star(value);
//    break;
    
    case('o'):
    rov_stop();
    break;
        //*********************************************************************
    /////////////////////////mechanism motion//////////////////////////
    case('c'):
    mechanism_up(value);
    break;

    case('v'):
    mechanism_down(value);
    break;

    case('z'):
    gripper_close(value);
    break;

    case('x'):
    gripper_open(value);
    break;
    //*********************** camera box *********************
    case('j'):
    Camera(value);
    break;
    
    case('k'):
    light(value);
    break;
    //****************************stop each trhruster**********
    case'$':
    h1_stop();
    break;
    
    case'%':
    h2_stop();
    break;
    
    case'^':
    h3_stop();
    break;

    case'&':
    v1_stop();
    break;

    case'*':
    v2_stop();
    break;
    //****************************stop all the rov motions**********
    case'@':
    rov_stop();
    mechanism_stop();
    break;

     //****************************stop manipulator**********
    case'#':
    mechanism_stop();
    break;

    default:
    rov_stop();
    mechanism_stop();
    break;
    }
     k+=3;
  }
}
