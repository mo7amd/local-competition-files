//********* Rov function is the main functon to control the motor *****************
void Rov(int val,byte h1,byte h2,byte h3,byte v1,byte v2,byte v3){
    int value;
    switch(h1){
    case(1):
    value = map(val,0,100,1464,r_speed_h1);
    mh1.writeMicroseconds(value);
    break;
    
    case(0):
    value = map(val,0,100,1464,f_speed_h1);
    mh1.writeMicroseconds(value);
    break;
   
    case(2):
    mh1.writeMicroseconds(1464);
    break;

    case (3):
    mh1.writeMicroseconds(1464/1.2);
    break;
    }
    
    switch(h2){
    case(1):
    value = map(val,0,100,1464,r_speed_h2);
    mh2.writeMicroseconds(value);
    break;
    
    case(0):
    value = map(val,0,100,1464,f_speed_h2);
    mh2.writeMicroseconds(value);
    break;
    
    case(2):
    mh2.writeMicroseconds(1464);
    break;

    case (3):
    mh2.writeMicroseconds(1464/1.2);
    break;
    }
    switch(h3){
    case(1):
    value = map(val,0,100,1464,r_speed_h2);
    mh3.writeMicroseconds(value);
    break;
    
    case(0):
    value = map(val,0,100,1464,f_speed_h2);
    mh3.writeMicroseconds(value);
    break;
    
    case(2):
    mh3.writeMicroseconds(1464);
    break;

    case (3):
    mh3.writeMicroseconds(1464/1.2);
    break;
    }

   switch(v1){
    case(1):
    value = map(val,0,100,1464,r_speed_v1);
    mv1.writeMicroseconds(value);
    break;
    
    case(0):
    value = map(val,0,100,1464,f_speed_v1);
    mv1.writeMicroseconds(value);
    break;
    
    case(2):
    mv1.writeMicroseconds(1464);
    break;

    case (3):
    mv1.writeMicroseconds(1464/1.2);
    break;
    }
    
     switch(v2){
    case(1):
    value = map(val,0,100,1464,r_speed_v1);
    mv2.writeMicroseconds(value);
    break;
    
    case(0):
    value = map(val,0,100,1464,f_speed_v1);
    mv2.writeMicroseconds(value);
    break;
    
    case(2):
    mv2.writeMicroseconds(1464);
    break;

    case (3):
    mv2.writeMicroseconds(1464/1.2);
    break;
    }
    switch(v3){
    case(1):
    value = map(val,0,100,1464,r_speed_v1);
    mv2.writeMicroseconds(value);
    break;
    
    case(0):
    value = map(val,0,100,1464,f_speed_v1);
    mv3.writeMicroseconds(value);
    break;
    
    case(2):
    mv3.writeMicroseconds(1464);
    break;

    case (3):
    mv3.writeMicroseconds(1464/1.2);
    break;
    }
   //Serial.println(val);
  }
  ///////////////////// all of Rov motions ////////////////////////////////
// Rov function take Speed and 6 int 
// mh1  mh2  mh3  mh4  mv1  mv0  
// 1:motor forward  ,  0:motor reverice  ,  2:motor stop
  void rov_forward(int Speed)
    {Rov(Speed,1,6,1,6,6,6);}
  
  void rov_backward(int Speed)
    {Rov(Speed,0,6,0,6,6,6);}
  
  void rov_lateral_r(int Speed)
    {Rov(Speed,6,1,6,6,6,6);}
  
  void rov_lateral_l(int Speed)
    {Rov(Speed,6,0,6,6,6,6);}
  
  void rov_rotate_cw(int Speed)
    {Rov(Speed,0,6,1,6,6,6);}
   
  void rov_rotate_ccw(int Speed)
    {Rov(Speed,1,6,0,6,6,6);}
  
  void rov_up(int Speed)
    {Rov(Speed,6,6,6,0,0,6);}
  
  void rov_down(int Speed)
    {Rov(Speed,6,6,6,1,1,6);}

   void rov_ForwardDiffR(int Speed)
    {Rov(Speed,0,3,6,6,6,6);}

   void rov_ForwardDiffL(int Speed)
    {Rov(Speed,3,0,6,6,6,6);}

//     void rov_BackDiffR(int Speed)
//    {Rov(Speed,1,3,2,2,2,2);}

//   void rov_BackDiffR(int Speed)
//    {Rov(Speed,3,1,2,2,2,2);}
//  void rov_horse(int Speed)
//    {Rov(Speed,2,2,1);}
//  
//  void rov_ampleh(int Speed)
//    {Rov(Speed,2,2,1);}
//  
//  void rov_dolfin(int Speed)
//    {Rov(Speed,1,2,1);}
// 
//  void rov_star(int Speed)
//     {Rov(Speed,1,2,1);}

  // stop all thrusters
  void rov_stop()
  {Rov(1464,2,2,2,2,2,2);}


  // stop h1
  void h1_stop()
  {Rov(1464,2,6,6,6,6,6);}
  // stop v2
  void h2_stop()
  {Rov(1464,6,2,6,6,6,6);}
  // stop h3
  void h3_stop()
  {Rov(1464,6,6,2,6,6,6);}
  // stop v1
  void v1_stop()
  {Rov(1464,6,6,6,2,6,6);}
  // stop v2
  void v2_stop()
  {Rov(1464,6,6,6,6,2,6);}

