/*
  draws indicators for the thrusters of the ROV in each 
  motion.
  green: forward
  blue: backward
  black: stop
*/

void rov_ui(int xPos, int yPos, char dir) {
  
  // default colors
  // 
  color stop = color(0, 0, 0);
  color forward = color(0, 255, 0);
  color reverse = color(0, 0, 255);
  // motor indicators
  color one = color(0, 0, 0);
  color two = color(0, 0, 0);
  color three = color(0, 0, 0);
  color four = color(0, 0, 0);
  color five = color(0, 0, 0);
  
  // get motors direction
  switch (dir) {
    // forward
    case 'w':
      one = stop;
      two = stop;
      three = stop;
      four = forward;
      five = forward;
      break;
      // backward
    case 's':
      one = stop;
      two = stop;
      three = stop;
      four = reverse;
      five = reverse;
      break;
      // lateral right
     case 'd':
      one = stop;
      two = forward;
      three = stop;
      four = stop;
      five = stop;
      break;
      // lateral left
     case 'a':
      one = stop;
      two = reverse;
      three = stop;
      four = stop;
      five = stop;
      break;
      // Rotate clockwise
     case 'e':
      one = stop;
      two = stop;
      three = stop;
      four = forward;
      five = reverse;
      break;
      // Rotate anti-clockwise
     case 'q':
      one = stop;
      two = stop;
      three = stop;
      four = reverse;
      five = forward;
      break;
      // up
     case 'u':
      one = forward;
      two = stop;
      three = forward;
      four = stop;
      five = stop;
      break;
       // down
     case 'n':
      one = reverse;
      two = stop;
      three = reverse;
      four = stop;
      five = stop;
      break;
      // stop all
      case 'o':
      one = stop;
      two = stop;
      three = stop;
      four = stop;
      five = stop;
      break;
    default:
      one = stop;
      two = stop;
      three = stop;
      four = stop;
      five = stop;
      break;
  }
  // motor1
  fill(one);
  noStroke();
  ellipse(xPos, yPos, 55, 55);
  // motor2
  fill(two);
  noStroke();
  rect(xPos - 55/2, yPos * 1.1 + 55 - 30/2, 55, 30);
  // motor3
  fill(three);
  noStroke();
  ellipse(xPos, yPos * 1.1 + 55 + 55 + 30/2, 55, 55);
  // motor4
  fill(four);
  noStroke();
  rect(xPos/1.2, yPos * 1.1 + 55 + 55 + 30/2, 30, 55);
  // motor5
  fill(five);
  noStroke();
  rect(xPos/0.9, yPos * 1.1 + 55 + 55 + 30/2, 30, 55);
}