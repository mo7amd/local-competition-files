void speed_mode() {
  if(forwardMaxSpeed == 50)
  {
    forwardMaxSpeed = 30;
    verticalForwardMaxSpeed = 35;
    speedModeString = "Low Speed Mode";
    bgColor = color(255, 255, 0);
  }
  else
  {
    forwardMaxSpeed = 50;
    verticalForwardMaxSpeed = 50;
    speedModeString = "High Speed Mode";
    bgColor = color(0, 255, 0);
  }
}