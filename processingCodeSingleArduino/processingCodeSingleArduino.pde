/*
              AquaPhoton Academy undergrad team's Client
 
 In order to use the client you need to follow
 download procontroll library from it's website or from the file I put on the group
 Place the procontroll library in the library folder
 In processing open sketch > import library > Add library then search for GUI, then press install on controlp5
 
 Important notes
 some things change when testing on different systems. e.g. windows to Linux
 The last time it was tested on Ubuntu so some variables might change it's name on windows as follows
 open examples > contributed libraries > procontroll > procontroll_printDevices
 connect the joystick
 run the example
 that will show you a list of all connected devices on processing terminal, even your mouse and keyboard
 scroll down to the joystick section
 You'll find different names for the sticks 
 you need to change these names for your xStck and yStick...etc
 
 */


// if joystick/gamepad is connected put true
boolean joystickConnected = true;
// if Arduinos are connected put true
boolean arduinoConnected = true;


boolean compansateFlag = false;

// add joystick library
import org.gamecontrolplus.gui.*;
import org.gamecontrolplus.*;
import net.java.games.input.*;
// base java class
import java.io.*;
// serial library
import processing.serial.*;
// gui library
import controlP5.*;
//import ipcapture.*;
//IPCapture cam;


// create objects
// GUI obj
ControlP5 cp5;
// checkbox obj
CheckBox checkbox;
// joystick obj
ControlIO control;
// add configuration file for the joystick
Configuration config;
// joystick instance
ControlDevice device;

// Right Arduino's obj
Serial arduinoRight;  // Create object from Serial class
// Left Arduino's Obj.
//Serial arduinoLeft;  // Create object from Serial class
// create logo obj
PImage logo;
// empty sting for the serial coming from the right Arduino
String inString = null;
// empty string for the serial from the Left Arduino
//String inString2 = null;
// empty character for loops
char inChar;

// toggle ROV's light
int lightR = 0;
String lightCondition = "OFF";

// speedToHold variable
String speedToHold = "none";

// variable that confirms if there's a speed in hold
boolean holdSpeed = false;

// booleans to stop and hold each motor
boolean disableH1 = false;
boolean disableH2 = false;
boolean disableH3 = false;
boolean disableV1 = false;
boolean disableV2 = false;

// gui positions and dimensions
// width of the GUI panel
int canvasWidth = 1020;
// height of the GUI panel/canvas
int canvasHeight = 700;
// other relative points on the canvas

int someRandomNumber = 0;


int logoX = canvasWidth/20;
int logoY = canvasHeight/30; // it's on the same line as the direction

int directionX = logoX + 50;
int directionY = logoY + 200;
// Virtual ROV's posititons
int rovX = floor(canvasWidth/2.15);
int rovY = floor(directionY * 2.5);

// default speeds
// max speed
int forwardMaxSpeed = 50;
int verticalForwardMaxSpeed = 50;
// stop speed
int stopSpeed = 10;

// Camera's servo position
int camera_position = 140;
String speedModeString = "High Speed Mode";

// global names for sensors
// temperature value
String tmpVal = "";
// current value
String currentVal = "";
// humidity value
String hVal = "";
// water value
String waterVal = "";
// pressure value
String pressureVal = "";

// bg color
color bgColor = color(0, 255, 0);

// read first arduino connected
String portName1 = Serial.list()[0];
// read second arduino connected
//String portName2 = Serial.list()[1];

void setup() {  
  // set the canvas/screen size
  size(300, 700);
  surface.setResizable(true);
  // anti-aleasing
  smooth(4);
  // set background color to white
  background(255);
  // load logo picture into it's object
  logo = loadImage("assets/img/teamLogo.png");
  // instantiate control obj
  cp5 = new ControlP5(this);
  
  // if  arduino is connected procced to Arduino related logic 
     // set arduino to it's port
    arduinoRight = new Serial(this, portName1, 9600);
    // flush serial from arduino 1
    arduinoRight.clear();
    //arduinoLeft = new Serial(this, portName2, 9600);
    //arduinoLeft.clear();
  
  // set color to black for all objects and text
  fill(0);
  // postition checkboxes`
  //checkbox = cp5.addCheckBox("checkBox")
  //  .setPosition(floor(rovX * 1.4), directionY * 1.2)
  //  .setSize(20, 20)
  //  .setItemsPerRow(1)
  //  .setSpacingColumn(30)
  //  .setSpacingRow(20)
  //  .addItem("0", 0)
  //  .addItem("50", 50)
  //  .addItem("100", 100)
  //  .addItem("150", 150)
  //  .addItem("200", 200)
  //  .addItem("255", 255)
  //  ;


  // Initialise the ControlIO
  control = ControlIO.getInstance(this);
  // check if the joystick is connected
  if (joystickConnected)
  {
    // add joystick name to the device obj
    device = control.getMatchedDevice("win-xbox");
   }
}

void draw() {
  //if (cam.isAvailable()) {
    //cam.read();
    //image(cam,0,0);
  //}
  // the following line to test the gui without joystick/gamepad 
  if (!joystickConnected)
  {
    // tests that are performed when the joystick is not connected. 
    // automatically disabled when flag is set true
    // stopped rov
    default_gui('o');
  }

  // make sure the joystick is connected before attempting to call methods on it's objects  
  if (joystickConnected)
  {
     //  --------------------------------------------------------------------- Forward and clockwise Block ----------------------------
    // move ROV forward and clockwise
    if (device.getSlider("forward-back").getValue() < 0 && device.getSlider("forward-back").getValue() >= -1 && device.getSlider("rotate").getValue() > 0 && device.getSlider("rotate").getValue() <= 1)
    {
     String fSpeed = str(int(map(device.getSlider("forward-back").getValue(), 0, -1, stopSpeed, forwardMaxSpeed)));
     
     if (arduinoConnected) {
       speedToHold = "t"+fSpeed;
       arduinoRight.write(speedToHold+"\n");
     } 
     // add ui elements for rov as indecators
     default_gui('w');
     text("Moving Forward-clockwise", directionX - 100, directionY);
     text(fSpeed, directionX + 50, directionY + 50);
    }
      //  --------------------------------------------------------------------- Forward and anti-clockwise Block ----------------------------
    // move ROV forward and anti-clockwise
    else if (device.getSlider("forward-back").getValue() < 0 && device.getSlider("forward-back").getValue() >= -1 && device.getSlider("rotate").getValue() < 0 && device.getSlider("rotate").getValue() >= -1)
    {
     String fSpeed = str(int(map(device.getSlider("forward-back").getValue(), 0, -1, stopSpeed, 25)) - 1/4 * map(device.getSlider("forward-back").getValue(), 0, -1, stopSpeed, forwardMaxSpeed));
     if (arduinoConnected) {
       speedToHold = "y"+fSpeed;
       arduinoRight.write(speedToHold+"\n");
       //arduinoLeft.write("w"+fSpeed+"\n");
     }
     // add ui elements for rov as indecators
     default_gui('w');
     text("Moving Forward anti-clockwise", directionX - 100, directionY);
     text(fSpeed, directionX + 50, directionY + 50);  
    }
    //  --------------------------------------------------------------------- Forward Block ----------------------------
    // move ROV forward w
    else if (device.getSlider("forward-back").getValue() < 0 && device.getSlider("forward-back").getValue() >= -1 && !disableH1 && !disableH2)
    {
      String fSpeed = str(int(map(device.getSlider("forward-back").getValue(), 0, -1, stopSpeed, forwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "w"+fSpeed;
        arduinoRight.write(speedToHold+"\n");
        //arduinoLeft.write("w"+fSpeed+"\n");
      }
      // add ui elements for rov as indecators
      default_gui('w');
      text("Moving Forward", directionX, directionY);
      text(fSpeed, directionX + 50, directionY + 50);  
    }
    


    //  --------------------------------------------------------------------- Backward Block ----------------------------
    // move ROV backward s
 else if (device.getSlider("forward-back").getValue() > 0 && device.getSlider("forward-back").getValue() <= 1 && !disableH1 && !disableH2)
    {
      String bSpeed = str(int(map(device.getSlider("forward-back").getValue(), 0, 1, stopSpeed, forwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "s"+bSpeed;
        arduinoRight.write(speedToHold+"\n");
      }
      default_gui('s');
      text("Moving backward", directionX, directionY);
      text(bSpeed, directionX + 50, directionY + 50);
    }

    //  --------------------------------------------------------------------- Lateral Right Block ----------------------------
    // move lateral right d 20 70
    else if (device.getSlider("lateral").getValue() > 0 && device.getSlider("lateral").getValue() <= 1 && !disableH3)
    {
      String lRight = str(int(map(device.getSlider("lateral").getValue(), 0, 1, stopSpeed, forwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "d"+lRight;
        arduinoRight.write(speedToHold+"\n");
        //arduinoLeft.write("d"+lRight+"\n");
      }
      default_gui('d');
      text("Moving lateral right", directionX, directionY);
      text(lRight, directionX + 50, directionY + 50);
    }

    //  --------------------------------------------------------------------- Lateral Left Block ----------------------------
    // move lateral left a 20 70
    else if (device.getSlider("lateral").getValue() >= -1 && device.getSlider("lateral").getValue() < 0 && !disableH3)
    {
      String lRight = str(int(map(device.getSlider("lateral").getValue(), 0, -1, stopSpeed, forwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "a"+lRight;
        arduinoRight.write(speedToHold+"\n");
        //arduinoLeft.write("a"+lRight+"\n");
      }
      default_gui('a');
      text("Moving lateral left", directionX, directionY);
      text(lRight, directionX + 50, directionY + 50);
    }
    //  --------------------------------------------------------------------- Rotate clockwise Block ----------------------------
    else if (device.getSlider("rotate").getValue() > 0 && device.getSlider("rotate").getValue() <= 1 && !disableH1 && !disableH2)
    {
      String speed = str(int(map(device.getSlider("rotate").getValue(), 0, 1, stopSpeed, forwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "q"+speed;
        arduinoRight.write(speedToHold+"\n");
      }
      default_gui('q');
      text("Rotating Clockwise", directionX - 20, directionY);
      text(speed, directionX + 50, directionY + 50);
    }

    //  --------------------------------------------------------------------- Rotate anti-clockwise Block ----------------------------
    else if (device.getSlider("rotate").getValue() < 0 && device.getSlider("rotate").getValue() >= -1 && !disableH1 && !disableH2)
    {
      String speed = str(int(map(device.getSlider("rotate").getValue(), 0, -1, stopSpeed, forwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "e"+speed;
        arduinoRight.write(speedToHold+"\n");
        
      }
      default_gui('e');
      text("Rotating Anti-Clockwise", directionX - 40, directionY);
      text(speed, directionX + 50, directionY + 50);
    }
    
    //  --------------------------------------------------------------------- Up with speed Block ----------------------------
    else if (device.getSlider("up_down").getValue() < 0 && device.getSlider("up_down").getValue() >= -1 && !disableV1 && !disableV2)
    {
      String speed = str(int(map(device.getSlider("up_down").getValue(), 0, -1, stopSpeed,verticalForwardMaxSpeed)));
      if (arduinoConnected) {
         speedToHold = "u"+speed;
         arduinoRight.write(speedToHold+"\n");
      }
      default_gui('u');
      text("Move Up", directionX, directionY);
      text(speed, directionX + 50, directionY + 50); 
    }

    //  --------------------------------------------------------------------- Down with speed Block ----------------------------
    else if (device.getSlider("up_down").getValue() > 0 && device.getSlider("up_down").getValue() <= 1 && !disableV1 && !disableV2)
    {
      String speed = str(int(map(device.getSlider("up_down").getValue(), 0, 1, stopSpeed,verticalForwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "n"+speed;
        arduinoRight.write(speedToHold+"\n");        
      }
      default_gui('n');
      text("Move Down", directionX, directionY);
      text(speed, directionX + 50, directionY + 50);
    }
    
      //  --------------------------------------------------------------------- Up with speed Block ----------------------------
    else if (device.getSlider("up_down2").getValue() < 0 && device.getSlider("up_down2").getValue() >= -1 && !disableV1 && !disableV2)
    {
      String speed = str(int(map(device.getSlider("up_down2").getValue(), 0, -1, stopSpeed,verticalForwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "u"+speed;
        arduinoRight.write(speedToHold+"\n");
      }
      default_gui('u');
      text("Move Up", directionX, directionY);
      text(speed, directionX + 50, directionY + 50); 
    }

    //  --------------------------------------------------------------------- Down with speed Block ----------------------------
    else if (device.getSlider("up_down2").getValue() > 0 && device.getSlider("up_down2").getValue() <= 1 && !disableV1 && !disableV2)
    {
      String speed = str(int(map(device.getSlider("up_down2").getValue(), 0, 1, stopSpeed,verticalForwardMaxSpeed)));
      if (arduinoConnected) {
        speedToHold = "n"+speed;
        arduinoRight.write(speedToHold+"\n");
      }
      default_gui('n');
      text("Move Down", directionX, directionY);
      text(speed, directionX + 50, directionY + 50);

    }
    
    //  --------------------------------------------------------------------- Stop Block ----------------------------
    else
    {
      
      if (arduinoConnected) {
        // stop all the thrusters one by one
        //arduinoRight.write("o\n");
        if (!disableH1)
        {
          arduinoRight.write("$\n");
        }
        if (!disableH2)
        {
          arduinoRight.write("%\n");
        }
        if (!disableH3)
        {
          arduinoRight.write("^\n");
        }
        if (!disableV1)
        {
          arduinoRight.write("&\n");
        }
        if (!disableV2)
        {
          arduinoRight.write("*\n");
        }
      }
      fill(255, 255, 255);
      textSize(22);
      default_gui('o');
      text("No Motion", directionX, directionY);
    }
    int timeCom = millis();
    int prevMillis = 0;
    // compansate speed to ease the job
    if(device.getButton("compansate_speed").pressed())
    {
      if(!holdSpeed && forwardMaxSpeed <= 30 && verticalForwardMaxSpeed <= 35 && timeCom >= prevMillis + 100) 
  {
    prevMillis = timeCom;
    String speedAtHold = speedToHold;
    holdSpeed = true;
    // activate the speed hold    
    // if a motion is at hold disable it's stop
    // get the speedToHold current motion
    switch(speedAtHold.charAt(0)) 
    {
      // get the first character in each motion
// forward and backward
      case 'w':
       disableH1 = true;
       disableH2 = true;
       //holdSpeed = true;
       arduinoRight.write(speedToHold+"\n");
       someRandomNumber++;
       //println(speedToHold + (someRandomNumber));
       break;
      case 's':
       disableH1 = true;
       disableH2 = true;
       arduinoRight.write(speedToHold+"\n");
       //holdSpeed = true;
       break;
// lateral right and left       
      case 'd':
       disableH3 = true;
       //holdSpeed = true;
       arduinoRight.write(speedToHold+"\n");
       break;
      case 'a':
       disableH3 = true;
       //holdSpeed = true;
       arduinoRight.write(speedToHold+"\n");
       break; 
// up and down       
      case 'u':
       disableV1 = true;
       disableV2 = true;
       //holdSpeed = true;
       arduinoRight.write(speedToHold+"\n");
       break;
      case 'n':
       disableV1 = true;
       disableV2 = true;
       //holdSpeed = true;
       arduinoRight.write(speedToHold+"\n");
       break;
     //default:
     //  holdSpeed = false;
    }
    arduinoRight.write(speedToHold+"\n");    
    holdSpeed = true;
  }
  else
  {
    //println("falseeeee");
   // release the speed hold
   // enable stop for all motors
   disableH1 = false;
   disableH2 = false;
   disableH3 = false;
   disableV1 = false;
   disableV2 = false;
   // toggle the variable
   //arduinoRight.write("o\n");
   holdSpeed = false;
  }
    }
    
    
     //  --------------------------------------------------------------------- Camera's Servo Block ----------------------------
    // Camera Up
    // servo up: hat = 2
    // servo down: hat = 6
    if (device.getHat("servo").up())
    {
      int speed = int(map(camera_position, 0, 180, 0, 180));
      // time in millis
      int press_time = millis();
      int last_pressed = 0;
      if (press_time - last_pressed > 1000)
      {
        if (camera_position >= 180) {
          camera_position = 180;
        } else {
          camera_position++;
        }    
        if (arduinoConnected) {
          arduinoRight.write("j"+speed+"\n");
         }
        default_gui('o');
        last_pressed = press_time;
      }
    }
    // Camera Down
    else if (device.getHat("servo").down())
    {
      int speed = int(map(camera_position, 0, 180, 0, 180));
      // time in millis
      int press_time = millis();
      int last_pressed = 0;
      if (press_time - last_pressed > 1000)
      {
        if (speed <= 0) {
          camera_position = 0;
        } else {
          camera_position--;
        }    
        if (arduinoConnected) {
          arduinoRight.write("j"+speed+"\n");
        }
        default_gui('o');
        last_pressed = press_time;
      }
    }
    //  --------------------------------------------------------------------- Manipulaltor Up Block ----------------------------
     // manipulator up
     if (device.getButton("mech_up").pressed())
     {
     String speed = str(100);
     if(arduinoConnected) {
     arduinoRight.write("c"+speed+"\n");
     }
     default_gui('o');
     text("Manipulator Up", directionX, directionY);
     text(speed, directionX + 50, directionY + 50);
     }

     //  --------------------------------------------------------------------- Manipulaltor Down Block ----------------------------
     // manipulator up
     else if (device.getButton("mech_down").pressed())
     {
     String speed = str(100);
     if(arduinoConnected) {
     arduinoRight.write("v"+speed+"\n");
     }
     default_gui('o');
     text("Manipulator Down", directionX - 20, directionY);
     text(speed, directionX + 50, directionY + 50);
     }

     //  --------------------------------------------------------------------- Manipulaltor open ----------------------------
     // manipulator up
     else if (device.getButton("mech_open").pressed())
     {
     String speed = str(100);
     if(arduinoConnected) {
     arduinoRight.write("z"+speed+"\n");
     }
     default_gui('o');
     text("Manipulator open", directionX - 20, directionY);
     text(speed, directionX + 50, directionY + 50);
     }

     //  --------------------------------------------------------------------- Manipulaltor close ----------------------------
     // manipulator up
     else if (device.getButton("mech_close").pressed())
     {
     String speed = str(100);
     if(arduinoConnected) {
     arduinoRight.write("x"+speed+"\n");
     }
     default_gui('o');
     text("Manipulator close", directionX - 20, directionY);
     text(speed, directionX + 50, directionY + 50);
     }
     
     else 
     {
     if(arduinoConnected) {
     arduinoRight.write("#"+"\n");
     }
     }

    // Toggle Light
    //device.getButton("light").plug("control_light", ControlIO.ON_PRESS);
    // Toggle Speed Mode
    device.getButton("speed_mode").plug("speed_mode", ControlIO.ON_PRESS);
    //device.getButton("compansate_speed").plug("hold_speed", ControlIO.ON_PRESS);
   //if (cam.isAvailable()) {
   // cam.read();
   // image(cam,0,0);
  //}//
}

  // check for incoming data
  //if (arduinoConnected) {
    // ceck if there's available data from either of the two Arduinos
    //if (arduinoRight.available() > 0) {
      // read incoming serial until new line
      // fillin forst string
      //inString = arduinoRight.readStringUntil(10);

      // check that there's a valid string in inString
      
      //if (inString != null) {
      //  // loop over the string   
      //  for (char letter : inString.toCharArray())
      //  {
      //    // get the starting char that's associated with a sensor reading
      //    inChar = letter;
      //    break;
      //  }
      //  // check sensor to read
      //  if (inChar == 't')
      //  {
      //    textSize(24);
      //    tmpVal = (join(split(inString, inChar), ""));
      //  }
      //  if (inChar == 'w')
      //  {
      //    textSize(24);
      //    waterVal = (join(split(inString, inChar), ""));
      //  }
      //  // current sensor
      //  if (inChar == 'c')
      //  {
      //    textSize(24);
      //    currentVal = (join(split(inString, inChar), ""));
      //  }
      //  // humidity sensor
      //  if (inChar == 'h')
      //  {
      //    textSize(24);
      //    hVal = (join(split(inString, inChar), ""));
      //  }
      //  // pressure val
      //  if (inChar == 'p')
      //  {
      //    textSize(24);
      //    pressureVal = (join(split(inString, inChar), ""));
      //  }
      //}
      // if text is sent from arduino 2
      //if (inString2 != null) {
      //  //  text(inString2, width - width/12, height - height/20);
      //  // loop over the string   
      //  for (char letter : inString2.toCharArray())
      //  {
      //    // get the starting char that's associated with a sensor reading
      //    inChar = letter;
      //    break;
      //  }
      //  // check sensor to read
      //  if (inChar == 't')
      //  {
      //    textSize(24);
      //    tmpVal = (join(split(inString2, inChar), ""));
      //  }
      //  if (inChar == 'w')
      //  {
      //    textSize(24);
      //    waterVal = (join(split(inString2, inChar), ""));
      //  }
      //  // current sensor
      //  if (inChar == 'c')
      //  {
      //    textSize(24);
      //    currentVal = (join(split(inString2, inChar), ""));
      //  }
      //  // humidity sensor
      //  if (inChar == 'h')
      //  {
      //    textSize(24);
      //    hVal = (join(split(inString2, inChar), ""));
      //  }
      //  // pressure val
      //  if (inChar == 'p')
      //  {
      //    textSize(24);
      //    pressureVal = (join(split(inString2, inChar), ""));
      //  }
      //}
    //}
  //}
}