/*
  Refreshment function
  I use this function to redrawthe whole GUI between transitions
*/

void default_gui(char dir) {
  // background color  
  background(255);
  
  // print the team logo
  image(logo, logoX, logoY, 200, 90);

  // speed mode indicator 
  textSize(20);  
  fill(bgColor);
  noStroke();
  rect(logoX, logoY + 100, 200, 50);
  fill(0);
  text(speedModeString, logoX + 10, logoY + 130);
  
  // add ui elements for rov as indecators
  // rov_ui(rovX, rovY, dir);
  
  // font color
  fill(0);
   
  // append mission planner
  //mission_planner(floor(rovX * 1.4), directionY);
  
  // add the camera's position
   text("Camera: "+camera_position, logoX + 30, logoY + 320);
   slider_gui(logoX, logoY + 345, 180, 3, camera_position);
   
   // hold speed indicator
   if (holdSpeed)
   {
     text ("Hold Speed " + speedToHold, logoX, logoY + 445);
   }
   
  // light condition
  //text("Lights "+ lightCondition, logoX + 30, logoY + 400);
  
  
  // copyright
  // © 2015 AquaPhoton Co. All rights reserved.
  textSize(11);
  text("© 2016 AquaPhoton Academy All rights reserved.", 10, height - 10);

  // default text size
  // keep last in the function
  textSize(20);
  sensor_ui();
  //int timeInMil = millis();
  //int prevTime = 0;
  //if(timeInMil >= prevTime + 1000)
  //{
  //  cam = new IPCapture(this, "http://192.168.1.245/Streaming/channels/1/picture", "admin", "12345");
  //  cam.start();
  //  prevTime = timeInMil;  
}
//  //if(cam.isAvailable())
  //{
  //  cam.read();
  //  image(cam,0,0);
  //}
  
  
//}