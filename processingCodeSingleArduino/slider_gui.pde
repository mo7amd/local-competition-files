// that function draws the slider indicator
void slider_gui(int x, int y, int l, int h, int val) {
  // slider should be black
  fill(0);
  // draw the rect
  noStroke();
  rect(x, y, l, h);
  
   // color the ball
  fill(255,0,0);
  // draw the color
  noStroke();
  ellipse(x+int(map(val, 0, 180, 0, 180)), y+3, 30, 30);
  fill(0);
}